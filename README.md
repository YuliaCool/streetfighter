# stage-2-es6-for-everyone

## Instalation

`npm install`

`npm run build`

`npm run start`

open http://localhost:8080/

About extra-features:
I have added some extra-features to this project:
1. Timer for The Fatal Attack for each of fighters.
2. Status information about attack:
 - Information when The Fatal Attack is available;
 - Information about gotten attack from other fighter;
 - Information if fighter is under block;
 - Information if game is over for fighter.
3. Back button on the arena.
4. Statistic information about winner after fight:
  - Health of winner in the end of game;
  - Count of attacks made by winner;
  - Count of The Fatal Attacks made by winner;
  - Count of gotten attacks made by other fighter;
  - Count of dodged attacks made by other fighter;
5. Cool cursor.
