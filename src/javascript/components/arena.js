import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import { fight } from './fight';
import { showWinnerModal } from '../components/modal/winner';
import { goToPreview } from './modal/winner';

const disabledTime = 10;

export function renderArena(selectedFighters) {
  const root = document.getElementById('root');
  const arena = createArena(selectedFighters);

  root.innerHTML = '';
  root.append(arena);

 fight(...selectedFighters)
 .then(function(result){
    showWinnerModal(result);
 });
}

function createArena(selectedFighters) {
  const arena = createElement({ tagName: 'div', className: 'arena___root' });
  const healthIndicators = createHealthIndicators(...selectedFighters);
  const fighters = createFighters(...selectedFighters);
  const backButton = createBackButton();
  
  arena.append(backButton, healthIndicators, fighters);
  return arena;
}

function createHealthIndicators(leftFighter, rightFighter) {
  const healthIndicators = createElement({ tagName: 'div', className: 'arena___fight-status' });
  const versusSign = createElement({ tagName: 'div', className: 'arena___versus-sign' });
  const leftFighterIndicator = createHealthIndicator(leftFighter, 'left');
  const rightFighterIndicator = createHealthIndicator(rightFighter, 'right');

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

function createHealthIndicator(fighter, position) {
  const { name } = fighter;
  const container = createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
  const fighterName = createElement({ tagName: 'span', className: 'arena___fighter-name' });
  const indicator = createElement({ tagName: 'div', className: 'arena___health-indicator' });
  const bar = createElement({ tagName: 'div', className: 'arena___health-bar', attributes: { id: `${position}-fighter-indicator` }});
  const statusInfoWrapper = createElement({ tagName: 'div', className: 'arena___fighter-info-wrapper' });
  const statusInfo = createElement({tagName: 'span', className: 'arena___fighter-info', attributes: {id: `${position}-fighter-info` }});
  const fatalAttackTimerStatus = createElement({tagName: 'span', className: 'arena___fighter-timer', attributes: {id: `${position}-fighter-timer-status` }});
  const fatalAttackTimer = createElement({tagName: 'span', className: 'arena___fighter-timer', attributes: {id: `${position}-fighter-timer` }});

  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  statusInfoWrapper.append(statusInfo);
  statusInfoWrapper.appendChild(document.createElement("br"));
  statusInfoWrapper.append(fatalAttackTimerStatus);
  statusInfoWrapper.append(fatalAttackTimer);
  container.append(statusInfoWrapper);

  return container;
}

 export function updateHealthIndicator(fighter, damage){
  let healthBarId = fighter.position + "-fighter-indicator";
  let currentWidth = document.getElementById(healthBarId).style.width;
  let currentWidthPersents;
  let updatedWidth;
  if (!currentWidth) 
    currentWidthPersents = 100;
  else 
    currentWidthPersents = parseFloat(currentWidth, 10);

  if (fighter.health > 0) {
    let damageInPersent = (damage * 100) / fighter.initialHealth;
    updatedWidth = currentWidthPersents - damageInPersent;
    if (updatedWidth < 0) 
      updatedWidth = 0;
  }
  else updatedWidth = 0;
  
  document.getElementById(healthBarId).style.width = updatedWidth + "%";
}

export function updateStatusInfo(fighter, text){
  let id = `${fighter.position}-fighter-info`;
  document.getElementById(id).innerText = text;
}

export function setTimer(fatalAttacker, textStatus = "Fatal attack will be available in "){
  let idTimer = `${fatalAttacker.position}-fighter-timer`;
  document.getElementById(idTimer).innerText = disabledTime;

  let idTimerStatus = `${fatalAttacker.position}-fighter-timer-status`;
  document.getElementById(idTimerStatus).innerText = textStatus;
  
}

function resetTimer(fatalAttacker, textStatus = "Fatal attack is available"){
  let id = `${fatalAttacker.position}-fighter-timer`;
  document.getElementById(id).innerText = '';

  let idTimerStatus = `${fatalAttacker.position}-fighter-timer-status`;
  document.getElementById(idTimerStatus).innerText = textStatus;
}

export async function updateTimer(fatalAttacker){
  let timerId = setInterval(function(){
    let id = `${fatalAttacker.position}-fighter-timer`;
    let currentSecond = parseInt(document.getElementById(id).innerHTML);
    document.getElementById(id).innerHTML = --currentSecond;
    if (currentSecond == 0){
      clearInterval(timerId);
      resetTimer(fatalAttacker);
      fatalAttacker.availableFatalAttack = true;
    }
  }, 1000);
}

function createFighters(firstFighter, secondFighter) {
  const battleField = createElement({ tagName: 'div', className: `arena___battlefield` });
  const firstFighterElement = createFighter(firstFighter, 'left');
  const secondFighterElement = createFighter(secondFighter, 'right');

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(fighter, position) {
  const imgElement = createFighterImage(fighter);
  const positionClassName = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
  const fighterElement = createElement({
    tagName: 'div',
    className: `arena___fighter ${positionClassName}`,
  });

  fighterElement.append(imgElement);
  return fighterElement;
}

function createBackButton(){
  const container = createElement({ tagName: 'div', className: 'arena___back-btn-wrapper' });
  const backBtn = createElement({
    tagName: 'button',
    className: 'arena___back-btn',
  });

  const onClick = goToPreview;
  backBtn.addEventListener('click', onClick, false);
  backBtn.innerText = 'Back';
  container.append(backBtn);
  return container;
}