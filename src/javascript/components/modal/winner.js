import { showModal } from './modal';
import App from '../../../javascript/app';

export function showWinnerModal(fighter) {
  let roundedHealth = Math.round(fighter.health * 100) / 100;
  let modalInfo = {
    title: `The winner is ${fighter.name}`, 
    bodyElement: `His health is ${roundedHealth} after fight. 
                  He has attacked ${fighter.countOfAttacks} times and ${fighter.countOfFatalAttack} times by fatal attack. 
                  He has got ${fighter.countOfFaults} hits from his opponent and dodged by ${fighter.countOfDodges} times.`,
    onClose: goToPreview
  };
  showModal(modalInfo);
}

export function goToPreview(){
  let div = document.getElementById("root");
  while(div.firstChild){
    div.removeChild(div.firstChild);
  }
  new App();
}