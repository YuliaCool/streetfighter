import { controls } from '../../constants/controls';
import { updateHealthIndicator, updateStatusInfo, setTimer, updateTimer } from './arena.js';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    let firstFighterOutfitted = {
      ...firstFighter,
      initialHealth: firstFighter.health,
      blocked: false,
      availableFatalAttack: true,
      position: "left",
      countOfAttacks: 0,
      countOfFatalAttack: 0,
      countOfFaults: 0,
      countOfDodges: 0
    };
    let secondFighterOutfitted = {
      ...secondFighter,
      initialHealth: secondFighter.health,
      blocked: false,
      availableFatalAttack: true,
      position: "right",
      countOfAttacks: 0,
      countOfFatalAttack: 0,
      countOfFaults: 0,
      countOfDodges: 0
    }

    let keysPressed = new Set();
    
    function handleAttack(e){
      let key = e.code;
      switch(key){
        case controls.PlayerOneAttack: {
          attack(firstFighterOutfitted, secondFighterOutfitted); 
          break;
        } 
        case controls.PlayerTwoAttack: {
          attack(secondFighterOutfitted, firstFighterOutfitted);
          break;
        }
        case controls.PlayerOneBlock: {
          block(firstFighterOutfitted);
          break;
        }
        case controls.PlayerTwoBlock: {
          block(secondFighterOutfitted);
          break;
        }
      }

      if (secondFighterOutfitted.health < 0) {
        document.removeEventListener('keydown', handleAttack);
        document.removeEventListener('keyup', handleFatalAttack);
        resolve(firstFighterOutfitted);
      }
      if (firstFighterOutfitted.health < 0) {
        document.removeEventListener('keydown', handleAttack);
        document.removeEventListener('keyup', handleFatalAttack);
        resolve(secondFighterOutfitted);
      }
    }

    async function handleFatalAttack(e){
      keysPressed.add(e.code);
      let fatalAttacker, fatalDefender;

      let firstFighterAttacks = checkCombination(controls.PlayerOneCriticalHitCombination, keysPressed);
      let secondFighterAttacks = checkCombination(controls.PlayerTwoCriticalHitCombination, keysPressed);
      if (firstFighterAttacks && firstFighterOutfitted.availableFatalAttack){
        fatalAttacker = firstFighterOutfitted;
        fatalDefender = secondFighterOutfitted;
      }
      if (secondFighterAttacks && secondFighterOutfitted.availableFatalAttack){
        fatalAttacker = secondFighterOutfitted;
        fatalDefender = firstFighterOutfitted;
      }

      if ((firstFighterAttacks && firstFighterOutfitted.availableFatalAttack) 
       || (secondFighterAttacks && secondFighterOutfitted.availableFatalAttack)){
        fatalAttack(fatalAttacker, fatalDefender);
        keysPressed.clear();
        if (secondFighterOutfitted.health < 0) {
          document.removeEventListener('keydown', handleAttack);
          document.removeEventListener('keyup', handleFatalAttack);
          resolve(firstFighterOutfitted);
        }
        if (firstFighterOutfitted.health < 0) {
          document.removeEventListener('keydown', handleAttack);
          document.removeEventListener('keyup', handleFatalAttack);
          resolve(secondFighterOutfitted);
        }

        fatalAttacker.availableFatalAttack = false;
        setTimer(fatalAttacker);
        await updateTimer(fatalAttacker);
      } 
    }
    document.addEventListener('keydown', handleAttack);
    document.addEventListener('keyup', handleFatalAttack);
  });
}

export function getDamage(attacker, defender) {
  let blockValue = (defender.blocked) ? getBlockPower(defender) : 0;
  let hitValue = getHitPower(attacker);
  let damage = blockValue >= hitValue ? 0 : hitValue - blockValue;
  return damage;
}

function getFatalDamage(attacker) {
  return 2 * attacker.attack;
}

export function getHitPower(fighter) {
  let criticalHitChance = Math.random() + 1;
  return criticalHitChance * fighter.attack;
}

export function getBlockPower(fighter) {
  let dodgeChance = Math.random() + 1;
  return dodgeChance * fighter.defense;
}

function attack(attacker, defender) {
  if(!attacker.blocked) {
    let damage = getDamage(attacker, defender);
    if (damage != 0){  
      attacker.countOfAttacks++;
      defender.countOfFaults++;
      updateHealthIndicator(defender, damage);
      let roundedDamage = getRoundedDamage(damage);
      defender.health -= damage;
      let massage = (defender.health > 0) ? `- ${roundedDamage} of health` : `Game over! :(`;
      updateStatusInfo(defender, massage);
    }
    else {
      updateStatusInfo(defender, 'Dodged a hit!');
      defender.countOfDodges++;
    }
  }
  else 
    updateStatusInfo(attacker, 'Unblock for attack');  
}

async function fatalAttack(attacker, defender){
  if(!attacker.blocked){
    attacker.countOfFatalAttack++;
    defender.countOfFaults++;
    let damage = getFatalDamage(attacker);
    updateHealthIndicator(defender, damage);
    let roundedDamage = getRoundedDamage(damage);
    defender.health -= damage;
    let massage = (defender.health > 0) ? `Damn it man! It is fatal attack - ${roundedDamage} of health` : `Game over! :(`;
    updateStatusInfo(defender, massage);
  }
  else 
    updateStatusInfo(attacker, 'Unblock for attack');
}


function block(defender){
  defender.blocked = !defender.blocked;
  let blockedText = defender.blocked ? "Under block" : "";
  updateStatusInfo(defender, blockedText);
}

function checkCombination(criticalHitCombination, pressedKeys){
  let countOfNeadedKeys = 0;
  for (let key of criticalHitCombination){
    if (pressedKeys.has(key)) 
      countOfNeadedKeys++;
  }
  if (criticalHitCombination.length == countOfNeadedKeys)
    return true;
  else 
    return false;
}

export function getRoundedDamage(damage){
  return Math.round(damage * 100) / 100;
}
