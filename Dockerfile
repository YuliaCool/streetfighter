FROM node:6-alpine

COPY dist ./dist
COPY index.html ./
COPY index.js ./
COPY package*.json ./
RUN npm install

CMD ["node", "index.js"]
EXPOSE 8080